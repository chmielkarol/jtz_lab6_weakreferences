package lab06;

import java.awt.Graphics;
import java.awt.image.BufferedImage;

import javax.swing.JPanel;

class ImagePanel extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3959810079334809997L;
	private BufferedImage image;
	
	public ImagePanel() {
		image = null;
	}
	
	public ImagePanel(BufferedImage image) {
		this.image = image;
	}
	
	public void setImage(BufferedImage image) {
		this.image = image;
		repaint();
	}
	
	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		if(image!=null) {
			double imageFactor = 1.0 * image.getWidth() / image.getHeight();
			double panelFactor = 1.0 * getWidth() / getHeight();
			
			int width = 0;
			int height = 0;
			int x = 0;
			int y = 0;

			//obraz szerszy od panelu lub o takich samych proporcjach
			if(imageFactor >= panelFactor) {
				width = getWidth();
				height = image.getHeight() * width / image.getWidth();
				y = (getHeight() - height) / 2;
			}
			else {
				height = getHeight();
				width = image.getWidth() * height / image.getHeight();
				x = (getWidth() - width) / 2;
			}
			g.drawImage(image, x, y, width, height, this);
		}
	}
}
