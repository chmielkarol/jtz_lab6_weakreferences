package lab06;

import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;
import javax.swing.UIManager;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import java.util.WeakHashMap;
import java.awt.event.ActionEvent;

public class Window {
	
	private JFileChooser dirChooser;
	
	private ImagePanel panel_1;
	private ImagePanel panel_2;

	private JFrame frmPrzegldarkaObrazw;
	
	private JButton btnUp;
	private JButton btnDown;
	
	private WeakHashMap<Integer, BufferedImage> images;
	private File[] files;
	
	private int index = -1;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Window window = new Window();
					window.frmPrzegldarkaObrazw.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Window() {
		//Directory chooser initialization
		dirChooser = new JFileChooser();
		dirChooser.setCurrentDirectory(new java.io.File("."));
		dirChooser.setDialogTitle("Wybierz folder");
		dirChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		dirChooser.setAcceptAllFileFilterUsed(false);
		
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmPrzegldarkaObrazw = new JFrame();
		frmPrzegldarkaObrazw.setResizable(false);
		frmPrzegldarkaObrazw.setTitle("Przegl\u0105darka obraz\u00F3w");
		frmPrzegldarkaObrazw.setBounds(100, 100, 382, 324);
		frmPrzegldarkaObrazw.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmPrzegldarkaObrazw.getContentPane().setLayout(null);
		
		JButton btnNewButton = new JButton("Wybierz folder");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int result = dirChooser.showOpenDialog(null);
				if(result == JFileChooser.APPROVE_OPTION) {
					File dir = dirChooser.getSelectedFile();
					images = new WeakHashMap<>();
					files = dir.listFiles(new FilenameFilter() {
						public boolean accept(File dir, String name) {
							return name.endsWith(".png") || name.endsWith(".jpg") || name.endsWith(".jpeg");
						}
					});
					for(int i = 0; i < files.length; ++i) {
						File file = files[i];
						try {
							images.put(i, ImageIO.read(file));
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
					
					index = 0;
					printImages();
				}
			}
		});
		btnNewButton.setBounds(54, 11, 204, 28);
		frmPrzegldarkaObrazw.getContentPane().add(btnNewButton);
		
		btnUp = new JButton("\u25B2");
		btnUp.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				--index;
				printImages();
				System.gc();
			}
		});
		btnUp.setEnabled(false);
		btnUp.setBounds(268, 105, 51, 23);
		frmPrzegldarkaObrazw.getContentPane().add(btnUp);
		
		btnDown = new JButton("\u25BC");
		btnDown.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				++index;
				printImages();
				System.gc();
			}
		});
		btnDown.setEnabled(false);
		btnDown.setBounds(268, 219, 51, 23);
		frmPrzegldarkaObrazw.getContentPane().add(btnDown);
		
		JPanel panel = new JPanel();
		panel.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Obrazy w folderze", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		panel.setBounds(54, 56, 204, 228);
		frmPrzegldarkaObrazw.getContentPane().add(panel);
		panel.setLayout(null);
		
		panel_1 = new ImagePanel();
		panel_1.setBounds(20, 22, 160, 90);
		panel.add(panel_1);
		
		panel_2 = new ImagePanel();
		panel_2.setBounds(20, 127, 160, 90);
		panel.add(panel_2);
	}
	
	private void printImages() {
		int more = files.length - index;
		
		if(more >= 1) {
			BufferedImage img = images.get(index);
			if(img == null) {
				System.out.println("DEBUG: WeakReference removed");
				try {
					img = ImageIO.read(files[index]);
					images.put(index, img);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			panel_1.setImage(img);
			
			if(more >= 2) {
				img = images.get(index+1);
				if(img == null) {
					System.out.println("DEBUG: WeakReference removed");
					try {
						img = ImageIO.read(files[index+1]);
						images.put(index, img);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				panel_2.setImage(img);
				
				if(index >= 1)
					btnUp.setEnabled(true);
				else btnUp.setEnabled(false);
				
				if(more >= 3)
					btnDown.setEnabled(true);
				else btnDown.setEnabled(false);
			}
		}
	}
}
